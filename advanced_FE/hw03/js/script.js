


let Hamburger = function (size, stuffing) {

    try {

        if (!size) {
            throw new HamburgerException('Please enter values: size and stuffing');
        }

        if (size != Hamburger.SIZE_SMALL && size != Hamburger.SIZE_LARGE) {

            throw new HamburgerException('Please enter correct size')
        }


        if (!stuffing) {
            throw new HamburgerException('Please enter some stuffing')
        }

        if (stuffing != Hamburger.STUFFING_CHEESE &&
            stuffing != Hamburger.STUFFING_SALAD && stuffing != Hamburger.STUFFING_POTATO) {
            throw new HamburgerException('Please enter correct stuffing')
        }


        this.size = size;
        this.stuffing = stuffing;

    }

    catch (exception) {
        console.log("Error detected: ", exception)
    }


    this.size = size;
    this.stuffing = stuffing;
    this.allTopping = []
};


Hamburger.detailMenu = {
    'size_small': {price: 50, cal: 20},
    'size_large': {price: 100, cal: 40},
    'stuffing_cheese': {price: 10, cal: 20},
    'stuffing_salad': {price: 20, cal: 5},
    'stuffing_potato': {price: 15, cal: 10},
    'topping_mayo': {price: 20, cal: 5},
    'topping_spice': {price: 15, cal: 0}
};


Hamburger.SIZE_SMALL = 'size_small';
Hamburger.SIZE_LARGE = 'size_large';
Hamburger.STUFFING_CHEESE = 'stuffing_cheese';
Hamburger.STUFFING_SALAD = 'stuffing_salad';
Hamburger.STUFFING_POTATO = 'stuffing_potato';
Hamburger.TOPPING_MAYO = 'topping_mayo';
Hamburger.TOPPING_SPICE = 'topping_spice';


Hamburger.prototype.addTopping = function (...topping) {
    try {
        if (topping.length < 1) {
            throw new HamburgerException('Please select at least one topping for adding')
        } else {
            for (let key = 0; key < topping.length; key++) {
                if (this.allTopping.indexOf(topping[key]) == -1) {

                    if (topping[key] == Hamburger.TOPPING_MAYO || topping[key] == Hamburger.TOPPING_SPICE) {
                        this.allTopping.push(topping[key])
                    } else {
                        throw new HamburgerException(`Choosen topping (${topping[key]}) is not a topping in list`);
                    }

                } else {
                    throw new HamburgerException(`Choosen topping (${topping[key]}) is already selected`);
                }
            }
        }

    }

    catch (exception) {
        console.log("You have a problem", exception)
    }

};


Hamburger.prototype.removeTopping = function (...topping) {
    try {
        if (!topping.length) {
            throw new HamburgerException('Please choose at least one topping for deleting');
        } else {

            for (let key = 0; key < topping.length; key++) {
                if (topping[key] != Hamburger.TOPPING_MAYO && topping[key] != Hamburger.TOPPING_SPICE) {
                    throw new HamburgerException(`Choosen topping (${topping[key]}) is not a topping in list`);
                }
                else if (this.allTopping.indexOf(topping[key]) == -1) {
                    throw new HamburgerException(`Choosen topping (${topping[key]}) is not in your hamburger`);
                }
            }

            for (let key = 0; key < this.allTopping.length; key++) {
                if (topping.indexOf(this.allTopping[key]) > -1) {

                    this.allTopping.splice(key, 1);

                }
            }


        }

    }
    catch (e) {
        console.log("You have a problem: ", e);

    }
};


Hamburger.prototype.getToppings = function () {

    if (this.allTopping.length > 0) {
        return this.allTopping;
    } else {
        return 'Topping list is empty';
    }

};


Hamburger.prototype.getSize = function () {

    if (this.size) {
        return this.size;
    } else {
        return 'Size is not selected';
    }
};


Hamburger.prototype.getStuffing = function () {
    if (this.stuffing) {
        return this.stuffing;
    } else {
        return 'Stuffing is not selected';
    }
};


Hamburger.prototype.calculatePrice = function () {
    let price = 0;
    price = Hamburger.detailMenu[this.size].price + Hamburger.detailMenu[this.stuffing].price;

    for (let key = 0; key < this.allTopping.length; key++) {
        price += Hamburger.detailMenu[this.allTopping[key]].price;
    }

    return price;
};



Hamburger.prototype.calculateCalories = function () {
    let cal = 0;
    cal = Hamburger.detailMenu[this.size].cal + Hamburger.detailMenu[this.stuffing].cal;

    for (let key = 0; key < this.allTopping.length; key++) {
        cal += Hamburger.detailMenu[this.allTopping[key]].cal;
    }

    return cal;
};


function HamburgerException(message) {
    this.message = message;

    return this
}


let hamb1 = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);

// console.log(hamb1);

// console.log(hamb1.getSize());
// console.log(hamb1.getToppings());


hamb1.addTopping(Hamburger.TOPPING_MAYO);

// console.log(hamb1);

hamb1.addTopping(Hamburger.TOPPING_SPICE);
console.log(hamb1);

console.log(hamb1.calculatePrice());

// console.log(hamb1);
//
// console.log(hamb1.getToppings());
//
// hamb1.removeTopping();
//
// console.log(hamb1);
//
// hamb1.removeTopping(Hamburger.STUFFING_POTATO);
//
// console.log(hamb1);
//
// hamb1.removeTopping(Hamburger.TOPPING_SPICE);
//
// console.log(hamb1);


