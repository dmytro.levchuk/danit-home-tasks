//
// function myFunction() {
//     var x = document.getElementById("myTopnav");
//     if (x.className === "topnav") {
//         x.className += " responsive";
//     } else {
//         x.className = "topnav";
//     }
// }


$('.fa-bars').addClass('block-active');

$('#btnOpen').click(function() {
    $('.fa-bars').removeClass('block-active');
    $('.fa-times').addClass('block-active');
    $('.topnav').addClass('block-active');

});

$('#btnClose').click(function() {
    $('.fa-bars').addClass('block-active');
    $('.fa-times').removeClass('block-active');
    $('.topnav').removeClass('block-active');

});