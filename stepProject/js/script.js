
$(document).ready(function(){

    // Search icon.
    $(document).find(".menu-search-field").hide();
    $(".menu-search-icon").click(function () {
        $(document).find(".menu-search-field").show(500);

    });


    $(".menu-top").on("click", "a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });



    //First Tab
    $("div.section-services-selectTabs").each(function () {
        var tmp = $(this);
        console.log($(tmp).find(".section-services-selectTabs-lineTabs li"));
        $(tmp).find(".section-services-selectTabs-lineTabs li").each(function (i) {
            $(tmp).find(".section-services-selectTabs-lineTabs li:eq("+i+") a").click(function(){
                var tab_id=i+1;
                $(tmp).find(".section-services-selectTabs-lineTabs li").removeClass("section-services-selectTabs-active");
                $(this).parent().addClass("section-services-selectTabs-active");
                $(tmp).find(".section-services-selectTabs-content > div").stop(false,false).hide();
                $(tmp).find(".section-services-selectTabs-tab"+tab_id).stop(false,false).show();
                return false;
            });
        });
    });


//    Second Tab Amazing works
    $(document).find(".section-amazing-list li").hide();
    $(document).find(".amazing-sk-wave").hide();
    $('#amazing-hide').hide();

    let size_li = $(".section-amazing-list li").length;
    let x = 12;

    if (x <= size_li) {
        $('#amazing-load-more').show();
        console.log('show');
        console.log(size_li);
    }
    else {
        $('#amazing-load-more').hide();
        console.log('hide');
        console.log(size_li);
    }
    $('.section-amazing-list li:lt('+x+')').show();

    j = 0;

    function loadAmazing () {
        $(document).find(".amazing-sk-wave").hide();
        size_li = $(".section-amazing-list li.amazing-item-"+j).length;


        x= (x+12 <= size_li) ? x+12 : size_li;


        $('.section-amazing-list > li').hide();
        $('.section-amazing-list li.amazing-item-'+j+':lt('+x+')').show();
        if (x === size_li) {
            $('#amazing-load-more').hide();
            $('#amazing-hide').show();
        }
        else {
            $('#amazing-load-more').show();
        }
    }

    $('#amazing-load-more').click(function () {
        $('#amazing-load-more').hide();
        $(document).find(".amazing-sk-wave").show();
        setTimeout(loadAmazing, 3000);

    });

    $('#amazing-hide').click(function () {
        x = 12;
        $('.section-amazing-list > li').hide();
        $('.section-amazing-list li.amazing-item-'+j+':lt('+x+')').show();
        $('#amazing-load-more').show();
        $('#amazing-hide').hide();
    });


    //
    $("div.section-amazing-select").each(function () {
        var tmp1 = $(this);
        $(tmp1).find(".section-amazing-tabs li").each(function (i) {
            $(tmp1).find(".section-amazing-tabs li:eq("+i+") a").click(function(){
                $('#amazing-hide').hide();
                var tab_id=i;
                j = tab_id;
                $(tmp1).find(".section-amazing-tabs li").removeClass("section-amazing-tabs-active");
                $(this).parent().addClass("section-amazing-tabs-active");
                $(tmp1).find(".section-amazing-list > li").stop(false,false).hide();

                size_li = $(".amazing-item-"+tab_id).length;
                x = 12;

                if (x <= size_li) {
                    $('#amazing-load-more').show();
                }
                else {
                    $('#amazing-load-more').hide();
                }
                $('.section-amazing-list li.amazing-item-'+tab_id+':lt('+x+')').show();
                return false;
            });
        });
    });


// Carousel

    var tmp4 = $("div.section-responces-items-block");
    $(tmp4).find(".section-responces-items > div").each(function (i) {
        $(tmp4).find(".section-responces-items > div:eq(" + i + ")").addClass("section-responces-item" + (i+1));
        console.log(i);
    });


    var tmp5 = $("div.carousel-wrapper");
    $(tmp5).find(".carousel-items > div").each(function (i) {
        $(tmp5).find(".carousel-items > div:eq(" + i + ")").attr("id", "face" + (i+1));
        console.log(i);
    });



    function reloadCarousel() {
        $("div.carousel-wrapper").each(function () {
            var tmp2 = $(this);
            $(tmp2).find(".carousel-items > div").each(function (i) {
                $(tmp2).find(".carousel-items div:eq("+i+") div").click(function(){
                    let att = $(this).parent().attr("id");
                    let numId = Number(att.substring(4));
                    $(tmp2).find(".carousel-items > div").removeClass("carousel-block-active");
                    $(this).parent().addClass("carousel-block-active");
                    faceChacked = numId;
                    sectionResponceHideShow(faceChacked);
                    return false;
                });
            });
        });
    }

    function sectionResponceHideShow (value) {
        $(document).find(".section-responces-items > div").stop(false,false).hide();
        $(document).find(".section-responces-item"+value).stop(false,false).show();
    }


    let faceChacked = 3;

//carousel-block-active

    $(document).find(".carousel-items > div").removeClass("carousel-block-active");
    $(document).find(".carousel-items > div#face" + faceChacked).addClass("carousel-block-active");


    let size_carousel = $(".carousel-items > div").length;
    console.log(size_carousel);

    sectionResponceHideShow(faceChacked);
    reloadCarousel();

//

    $(document).on('click', ".carousel-button-right",function(){
        var carusel = $(this).parents('.carousel');
        right_carusel(carusel);
        sectionResponceHideShow(faceChacked);
        return false;
    });
    $(document).on('click',".carousel-button-left",function(){
        var carusel = $(this).parents('.carousel');
        left_carusel(carusel);
        sectionResponceHideShow(faceChacked);
        return false;
    });

    function left_carusel(carusel){
        var block_width = $(carusel).find('.carousel-block').outerWidth();
        $(carusel).find(".carousel-items .carousel-block").eq(-1).clone().prependTo($(carusel).find(".carousel-items"));
        $(carusel).find(".carousel-items").css({"left":"-"+block_width+"px"});
        $(carusel).find(".carousel-items .carousel-block").eq(-1).remove();
        $(carusel).find(".carousel-items").animate({left: "0px"}, 500);

        faceChacked = (faceChacked - 1 < 1) ? size_carousel : faceChacked - 1;

        $(document).find(".carousel-items > div").removeClass("carousel-block-active");
        $(document).find(".carousel-items > div#face" + faceChacked).addClass("carousel-block-active");
        reloadCarousel();

    }
    function right_carusel(carusel){
        var block_width = $(carusel).find('.carousel-block').outerWidth();
        $(carusel).find(".carousel-items").animate({left: "-"+ block_width +"px"}, 500, function(){
            $(carusel).find(".carousel-items .carousel-block").eq(0).clone().appendTo($(carusel).find(".carousel-items"));
            $(carusel).find(".carousel-items .carousel-block").eq(0).remove();
            $(carusel).find(".carousel-items").css({"left":"0px"});
            reloadCarousel();
        });

        faceChacked = (faceChacked + 1 > size_carousel) ? 1 : faceChacked + 1;

        $(document).find(".carousel-items > div").removeClass("carousel-block-active");
        $(document).find(".carousel-items > div#face" + faceChacked).addClass("carousel-block-active");


    }



//    Gallery Masonry

    // function showMasonry() {
    //     $('.section-gallery-block').masonry({
    //         // options
    //         itemSelector: '.gallery-item',
    //         columnWidth: 370,
    //         gutter: 15
    //     });
    // }

    function showMasonry() {
        var $grid = $('.section-gallery-block').imagesLoaded(function () {
            // init Masonry after all images have loaded
            $grid.masonry({
                // options
                itemSelector: '.gallery-item',
                columnWidth: 370,
                gutter: 15
            });
        });
    }


    function loadMasonry () {
        size_img = $(".section-gallery-block .gallery-item").length;
        x_img= (x_img+6 <= size_img) ? x_img+6 : size_img;
        $(document).find(".gallery-sk-wave").hide();
        $('.section-gallery-block .gallery-item').hide();
        $('.section-gallery-block .gallery-item:lt('+x_img+')').show();
        showMasonry();
        if (x_img === size_img) {
            $('#gallery-load-more').hide();
        }
        else {
            $('#gallery-load-more').show();
        }
    }


    $(document).find(".section-gallery-block .gallery-item").hide();
    $(document).find(".gallery-sk-wave").hide();

    let size_img = $(".section-gallery-block .gallery-item").length;
    let x_img = 9;


    if (x_img <= size_img) {
        $('#gallery-load-more').show();
    }
    else {
        $('#gallery-load-more').hide();
    }

    $('.section-gallery-block .gallery-item:lt('+x_img+')').show();
    showMasonry();

    $('#gallery-load-more').click(function () {
        $('#gallery-load-more').hide();
        $(document).find(".gallery-sk-wave").show();
        setTimeout(loadMasonry, 2000);
    });





});
