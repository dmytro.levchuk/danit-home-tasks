
function checkField (value) {

    while (value == null || value === "") {
        value = prompt("Empty field, please enter again", "");
    }
    return value;

}


function createNewUser() {

    this.firstName = "";
    this.lastName = "";
    Object.defineProperty(this, "firstName", {
        writable: false
    });

    Object.defineProperty(this, "lastName", {
        writable: false
    });



    this.setFirstName = function (name) {
        Object.defineProperty(this, "firstName", {
            writable: true
        });
        this.firstName = name;
    };

    this.setLastName = function (surname) {
        Object.defineProperty(this, "lastName", {
            writable: true;
        });
        this.lastName = surname;
    };

    this.getLogin = function () {
        console.log(this.firstName + " " + this.lastName);

        return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    };

    // this.firstName = name;
    // this.lastName = surname;



}

let name = prompt("Please enter first name", "");
name = checkField(name);
let surname = prompt("Please enter last name", "");
surname = checkField(surname);


let obj = new createNewUser();

// obj.firstName = name;

obj.setFirstName(name);

obj.setLastName(surname);

console.log(obj.getLogin());
