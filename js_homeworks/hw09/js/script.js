let dateOfBirth = prompt("Please enter your Birthday in format dd.mm.yyyy", "");

let dayOfBirth = Number(dateOfBirth.substring(0, 2));
let monOfBirth = Number(dateOfBirth.substring(3, 5)) - 1;
let yearOfBirth = Number(dateOfBirth.substring(6, 10));


let currentDate = new Date();
let currentDay = currentDate.getDate();
let currentMonth = currentDate.getMonth();
let currentYear = currentDate.getFullYear();

let arrChina = ["Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake",
    "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"];

let result;


if (currentMonth > monOfBirth) {
    result = currentYear - yearOfBirth;
}
else if (currentMonth < monOfBirth) {
    result = currentYear - yearOfBirth - 1;
}
else {
    if (currentDay >= dayOfBirth) {
        result = currentYear - yearOfBirth;
    }
    else {
        result = currentYear - yearOfBirth - 1;
    }

}

alert("You are " + result + " years old. You were born in year of " + arrChina[(yearOfBirth % 12) - 4] + " in Chinese astrology");

