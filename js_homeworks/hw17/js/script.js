$('#btn1').click(function () {
    $('#btn1').hide();
    $('#btn2').hide();

    let elemForm = $("<form></form>");

    let fieldOne = $("<p></p>");
    let textFieldOne = $("<span></span>");
    textFieldOne.text('Please enter an radius in pixels ');
    let inputFieldOne = $("<input>");
    inputFieldOne.attr('id', 'fld1');

    let fieldTwo = $("<p></p>");
    let textFieldTwo = $("<span></span>");
    textFieldTwo.text('Please enter a color of circle ');
    let inputFieldTwo = $("<input>");

    inputFieldTwo.attr('id', 'fld2');

    let submitBtn = $('<input>');
    submitBtn.attr('id', 'sbmBtn');
    submitBtn.attr('type', 'button');
    submitBtn.attr('value', 'And finally we can draw a circle');

    fieldOne.append(textFieldOne, inputFieldOne);
    fieldTwo.append(textFieldTwo, inputFieldTwo);

    elemForm.append(fieldOne, fieldTwo, submitBtn);

    $('body').append(elemForm);

    $('#sbmBtn').click(function () {

        let circleDiv = $('<div></div>');
        circleDiv.attr('style', `height: ${fld1.value}px; width: ${fld1.value}px; background-color: ${fld2.value}; border-radius: ${fld1.value / 2}px`);
        $('body').append(circleDiv);

    });


});


$('#btn2').click(function () {
    $('#btn1').hide();
    $('#btn2').hide();

    let elemForm = $("<form></form>");

    let fieldOne = $("<p></p>");
    let textFieldOne = $("<span></span>");
    textFieldOne.text('Please enter an radius in pixels ');
    let inputFieldOne = $("<input>");
    inputFieldOne.attr('id', 'fld1');

    let submitBtn = $('<input>');
    submitBtn.attr('id', 'sbmBtn');
    submitBtn.attr('type', 'button');
    submitBtn.attr('value', 'And finally we can draw a circle');

    fieldOne.append(textFieldOne, inputFieldOne);

    elemForm.append(fieldOne, submitBtn);

    $('body').append(elemForm);

    $('#sbmBtn').click(function () {
        let circles = $('<div></div>');
        circles.attr('id', 'circles');

        circles.attr('style', `width: ${fld1.value * 10}px`);
        for (let i = 0; i < 100; i++) {
            let circleDiv = $('<div></div>');

            circleDiv.attr('style', `float: left; height: ${fld1.value}px; width: ${fld1.value}px; 
            background-color: rgb(${Math.floor((Math.random() * 255))},${Math.floor((Math.random() * 255))},${Math.floor((Math.random() * 255))}); 
            border-radius: ${fld1.value / 2}px`);

            circles.append(circleDiv);
        }

        $('body').append(circles);

        circles.click (function (e) {
            $(e.target).hide();
        })


    });


});