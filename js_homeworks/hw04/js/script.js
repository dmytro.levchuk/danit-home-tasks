function startFibo(val1, val2, n) {
    if (n == 0) {
        nextVal = val1;
        return nextVal;
    }
    if (n == 1) {
        nextVal = val2;
        return nextVal;
    }
    else {
        if (n > 0) {
            nextVal = startFibo(val1, val2, n - 1) + startFibo(val1, val2, n - 2);
        }
        else {
            nextVal = startFibo(val1, val2, n + 2) - startFibo(val1, val2, n + 1);
        }
        return nextVal;
    }

}

let firstVal = 3;
let secondVal = 4;
let n = Number(prompt("Please enter number in sequence", ""));
let nextVal = 0;
console.log(n);

let res = startFibo(firstVal, secondVal, n);
console.log(res);

