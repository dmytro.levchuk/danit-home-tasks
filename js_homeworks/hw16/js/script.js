if (!localStorage["themeSite"]) {
    localStorage["themeSite"] = 'css/work7_1.css';
}

let btn = document.getElementById('btn1');
let fileCss = document.getElementById('linkStyles');
fileCss.setAttribute("href", localStorage["themeSite"]);

btn.onclick = function () {
    let currentAttr = fileCss.getAttribute('href');
    if (currentAttr == "css/work7_2.css") {
        fileCss.setAttribute("href", "css/work7_1.css");
        localStorage["themeSite"] = "css/work7_1.css";
    } else {
        fileCss.setAttribute("href", "css/work7_2.css");
        localStorage["themeSite"] = "css/work7_2.css";
    }
};
