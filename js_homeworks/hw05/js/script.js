function copyArr(arr) {
    let newArr = [];

    for (let i = 0; i < arr.length; i++) {

        if (typeof(arr[i]) === 'object') {
            if (Array.isArray(arr[i])) {
                newArr[i] = copyArr(arr[i]);
            }
            else {
                newArr[i] = copyObj.call(arr[i]);
            }
        }
        else {
            newArr[i] = arr[i];
        }

    }

    return newArr;
}


function copyObj() {
    let newObj = {};

    for (let key in this) {

        if (typeof(this[key]) === 'object') {
            if (Array.isArray(this[key])) {
                newObj[key] = copyArr(this[key]);
            }
            else {
                newObj[key] = copyObj.call(this[key]);
            }
        }
        else {
            newObj[key] = this[key];
        }
    }
    return newObj;

}


let obj = {
    name: "First obj",
    type: "simply",
    capacity: 4,
    array1: [1, 2, 3, 4],
    obj1: {
        name1: "Second obj",
        type2: "diff",
        array2: [{name2: "Name 2", type2: "Type 2"}, {name3: "Name 3", type3: "Type 3"}]
    }
};


if (typeof(obj) === 'object') {
    console.log(obj);

}
let secondObj = copyObj.call(obj);

obj.capacity = 5;
obj.array1[1] = 10;

obj.obj1.array2[1].type3 = "Type 33";

console.log(secondObj);



