$(document).ready(function () {

    let prevValue = 0;
    let currentValue = 0;
    let flagPoint = 0; // check for double dot in field
    let storeAction = '';
    let numberMemory = 0;
    let clickMrc = 0;

    $('.memory').removeClass('memory-active');


    let tmpEvent = $('.keys');

    let valueStr = '';
    $('#screen').val(valueStr);



    function callClick (whatBtn) {

        if (whatBtn == Number(whatBtn) || (whatBtn == '.' && flagPoint == 0)) {

            clickMrc = 0;

            if (whatBtn == '.') {
                flagPoint = 1;
            }

            // console.log('number');
            if (!(whatBtn != '.' && valueStr == '0')) {
                valueStr = valueStr + whatBtn;

            }

            $('#screen').val(valueStr);

            currentValue = Number(valueStr);
            // console.log('prev = ' + prevValue + ' current = ' + currentValue);
        }
        else if (whatBtn == '-' || whatBtn == '+' || whatBtn == '/' || whatBtn == '*' || whatBtn == '=') {

            clickMrc = 0;
            switch (storeAction) {

                case '-':
                    currentValue = prevValue - currentValue;
                    $('#screen').val(currentValue);
                    // console.log('minus');
                    break;
                case '+':
                    currentValue = prevValue + currentValue;
                    $('#screen').val(currentValue);
                    // console.log('plus');
                    break;
                case '*':
                    currentValue = prevValue * currentValue;
                    $('#screen').val(currentValue);
                    // console.log('multiply');
                    break;
                case '/':
                    currentValue = prevValue / currentValue;
                    $('#screen').val(currentValue);
                    // console.log('divide');
                    break;
                default:
                    break;


            }


            storeAction = whatBtn;
            prevValue = currentValue;
            valueStr = '';
            flagPoint = 0;


        }

        else if (whatBtn == 'm+' || whatBtn == 'm-') {

            clickMrc = 0;

            if (storeAction == '=') {
                numberMemory = currentValue;
                prevValue = 0;
                currentValue = 0;
                $('.memory').addClass('memory-active');
                valueStr = '';
                console.log(numberMemory);

            }


        }

        else if (whatBtn = 'mrc') {

            clickMrc += 1;

            if (clickMrc == 2) {
                numberMemory = 0;
                $('.memory').removeClass('memory-active');
                valueStr = '';
                currentValue = 0;
                $('#screen').val(valueStr);

            }

            else {
                if (storeAction == '-' || storeAction == '+' || storeAction == '/'
                    || storeAction == '*' || storeAction == '=') {

                    valueStr = numberMemory;
                    currentValue = numberMemory;
                    $('#screen').val(valueStr);

                }

            }


        }


    }



    $(tmpEvent).click(function (e) {

        let whatBtn = $(e.target).attr('value');

        callClick(whatBtn);



    });


    let arrKeys = {
        48: 0, 49: 1, 50: 2, 51: 3, 52: 4, 53: 5, 54: 6,
        55: 7, 56: 8, 57: 9, 42: '*', 43: "+",
        45: "-", 47: '/', 61: "="

    };


    $("#screen").keypress(function (e) {
        console.log("Handler for .keypress() called." + e.which);

        callClick(arrKeys[e.which]);

        // console.log(arrKeys[e.which]);
    });


});

