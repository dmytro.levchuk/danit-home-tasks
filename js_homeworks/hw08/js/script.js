function checkSubObject(subObj, subKey, keyWord, currentCount) {

    let countWord = currentCount;
    let checkPoint = subKey.indexOf(".");

    if (checkPoint == -1) {

        if (!Array.isArray(subObj)) {

            if (subKey in subObj) {

                if (subObj[subKey].indexOf(keyWord) >= 0) {
                    countWord++;
                }
            }


        }
        else {
            console.log("is arr");
            for (let i = 0; i < subObj.length; i++) {

                countWord = checkSubObject(subObj[i], subKey, keyWord, countWord);
            }
        }
    }

    else {
        let mainKey = subKey.substring(0, checkPoint);
        let addKey = subKey.substring(checkPoint + 1);

        if (mainKey in subObj) {

            countWord = checkSubObject(subObj[addKey], addKey, keyWord, countWord);

        }
    }
    return countWord;

}


function filterCollection(arrObj, keyWords, flag) {

    let filterArr = [];
    let indexFilter = 0;
    let checkFilter = 0;

    let arrKeys = [];
    let indexArrKeys = 0;
    for (let i = 3; i < arguments.length; i++) {
        arrKeys[indexArrKeys] = arguments[i];
        indexArrKeys++;
    }

    let arrWords = keyWords.split(" ");
    let arrWordsIs = [];


    for (let n = 0; n < arrObj.length; n++) {

        for (let p = 0; p < arrWords.length; p++) {
            arrWordsIs[p] = 0;
        }

        for (let i = 0; i < arrWords.length; i++) {


            for (let j = 0; j < arrKeys.length; j++) {

                let isSubKey = arrKeys[j].indexOf(".");

                if (isSubKey === -1) {

                    if (arrKeys[j] in arrObj[n]) {

                        if (arrObj[n][arrKeys[j]].indexOf(arrWords[i]) >= 0) {
                            arrWordsIs[i]++;
                        }
                    }
                }

                else {

                    let mainKey = arrKeys[j].substring(0, isSubKey);
                    let subKey = arrKeys[j].substring(isSubKey + 1);

                    if (mainKey in arrObj[n]) {

                        arrWordsIs[i] = checkSubObject(arrObj[n][mainKey], subKey, arrWords[i], arrWordsIs[i]);
                    }

                }

            }
        }


        for (let i = 0; i < arrWordsIs.length; i++) {
            if (arrWordsIs[i] > 0) {
                checkFilter++;
            }
        }

        if (flag == true && checkFilter == arrWordsIs.length) {
            filterArr[indexFilter] = arrObj[n];
            indexFilter++;
        }
        else if (flag == false && checkFilter > 0) {
            filterArr[indexFilter] = arrObj[n];
            indexFilter++;
        }

        checkFilter = 0;

    }

    return filterArr;

}


let carsArray = [
    {
        name: "Honda",
        model: "Civic",
        country: "Japan",
        type: "hatchback",
        engine: [
            {
                capacity: "1.8 l",
                liquid: "petrol",
                type: "turbo"
            },
            {
                capacity: "1.8 l",
                liquid: "petrol",
                type: "turbo"
            }
        ]
    },
    {
        name: "Honda",
        model: "Accord",
        country: "Japan",
        type: "sedan"
    },
    {
        name: "Toyota",
        model: "Yaris",
        country: "Japan",
        type: "hatchback"
    },
    {
        name: "BMW",
        model: "530i",
        country: "Ukraine",
        type: "sedan"
    }
];


let toFind = "on apa an etr";
let allOrNot = false;


let filtArr = filterCollection(carsArray, toFind, allOrNot, 'name', 'country', 'engine.liquid');
console.log(carsArray);
console.log(filtArr);

