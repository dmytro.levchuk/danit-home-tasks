function excludeBy(arrayMain, arrayExc, value) {
    let readyArr = [];
    let indexReadyArr = 0;
    let checkIs = 0;

    for (let i = 0; i < arrayMain.length; i++) {

        for (let j = 0; j < arrayExc.length; j++) {

            if (arrayMain[i][value] === arrayExc[j][value]) {
                checkIs = 1;
            }

        }

        if (checkIs == 0) {
            readyArr[indexReadyArr] = arrayMain[i];
            indexReadyArr++;

        }
        else {
            checkIs = 0;
        }

    }

    return readyArr;


}


const users1 = [
    {
        name: "Ivan",
        surname: "Ivanov",
        gender: "male",
        hobby: "football",
        married: "yes",
        age: 30
    },
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        worker: "IT",
        married: "no",
        age: 22
    },
    {
        name: "Masha",
        surname: "Shevchenko",
        gender: "female",
        age: 18
    },
    {
        name: "Tonya",
        surname: "Matvienko",
        gender: "female",
        hobby: "tenis",
        married: "yes",
        age: 25
    },
    {
        name: "Andriy",
        surname: "Kapustin",
        gender: "female",
        worker: "study",
        age: 27
    }


];


const users2 = [
    {
        name: "Sasha",
        surname: "Ivanov",
        gender: "male",
        married: "no",
        age: 30
    },
    {
        name: "Anna",
        surname: "Poroshenko",
        gender: "female",
        worker: "IT",
        hobby: "football",
        married: "no",
        age: 29
    },
    {
        name: "Ira",
        surname: "Shevchenko",
        gender: "female",
        age: 18
    },
    {
        name: "Dmytro",
        surname: "Matvienko",
        gender: "male",
        married: "no",
        age: 28
    },
    {
        name: "Andriy",
        surname: "Topolya",
        gender: "female",
        hobby: "snooker",
        worker: "study",
        age: 27
    }

];


let field = prompt("Please enter key in objects for compare", "");

let newArr = excludeBy(users1, users2, field);

console.log(newArr);