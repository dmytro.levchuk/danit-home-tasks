$(document).ready(function () {

    class stopWatch {
        constructor() {
            this.countClick = 0;
            this.realCounter = 0;
            this.realStart = 0;
            this.realStop = 0;
            this.realDiff = 0;
            this.count = 0;
            this.timer = 0;

        }


        drawWatch(value) {
            let countMs = value % 1000;
            let countSec = ((value - countMs) / 1000) % 60;
            let countMin = (((value - countMs) / 1000) - countSec) / 60;
            countMin = (Number(countMin) < 10) ? '0' + countMin : countMin;
            countSec = (Number(countSec) < 10) ? '0' + countSec : countSec;

            if (Number(countMs) < 10) {
                countMs = '00' + countMs;
            }
            else if (Number(countMs) < 100) {
                countMs = '0' + countMs;
            }

            if (this.type == 0) {
                this.countDiv.innerHTML = countMin + '.' + countSec + ',' + countMs;
            }
            else {
                console.log('hello stop');
                this.secs.style = `transform: rotate(${(-90) + countSec * 6}deg);`;
                this.mins.style = `transform: rotate(${(-90) + countMin * 6}deg);`;
                this.mSecs.innerHTML = countMs;
            }

            return true;
        }


        counterStep() {
            this.count += 4;
            this.drawWatch(this.count);
        }


        startFunc() {
            let that = this;
            if (!this.countClick) {
                this.realStart = new Date().getTime();

                this.timer = setInterval((function () {
                    that.counterStep();
                }), 4);

                this.countClick = 1;
            }

            return true;

        }

        stopFunc() {

            this.realStop = new Date().getTime();
            this.realDiff = this.realStop - this.realStart;
            this.realCounter += this.realDiff;
            this.drawWatch(this.realCounter);
            clearInterval(this.timer);
            this.countClick = 0;

            return true;
        }

        resetFunc() {
            this.countClick = 0;
            this.count = 0;
            this.realCounter = 0;
            this.drawWatch(this.realCounter);
            clearInterval(this.timer);
        }


    }


    class simpleStopWatch extends stopWatch {
        constructor() {
            super();
            this.type = 0;
            this.countDiv = document.getElementById('viewCounter');
        }
    }

    class rolexStopWatch extends stopWatch {
        constructor() {
            super();
            this.type = 1;
            this.mins = document.getElementById('arrowMins');
            this.secs = document.getElementById('arrowSecs');
            this.mSecs = document.getElementById('showMsecs');
        }
    }


    let counter = new simpleStopWatch();

    $('#btnStart').show();
    $('#btnStop').hide();

    btnStart.onclick = function () {
        $('#btnStart').hide();
        $('#btnStop').show();
        counter.startFunc();
    };

    btnStop.onclick = function () {
        $('#btnStart').show();
        $('#btnStop').hide();
        counter.stopFunc();
    };

    btnReset.onclick = function () {
        $('#btnStart').show();
        $('#btnStop').hide();
        counter.resetFunc();
    };


    let counterRolex = new rolexStopWatch();

    $('#btnStopRolex').hide();

    btnStartRolex.onclick = function () {
        $('#btnStartRolex').hide();
        $('#btnStopRolex').show();
        counterRolex.startFunc();
    };


    btnStopRolex.onclick = function () {
        $('#btnStartRolex').show();
        $('#btnStopRolex').hide();
        counterRolex.stopFunc();
    };

    btnResetRolex.onclick = function () {
        $('#btnStartRolex').show();
        $('#btnStopRolex').hide();
        counterRolex.resetFunc();
    };


});