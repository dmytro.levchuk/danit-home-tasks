function factorial(x) {
    if (x == 1) {
        return x;
    }
    else {
        return x*factorial(x-1);
    }
}


function checkInteger (value) {

    while (!Number.isInteger(Number(value)) || Number(value) < 1) {
        value = prompt ("Number must be integer, please enter again", "");
    }
    return Number(value);

}


let value = prompt ("Please enter a number", "");
value = checkInteger(value);

let fact = factorial(value);

console.log(fact);