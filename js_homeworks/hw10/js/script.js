function checkPossibility(arrPeople, arrTasks, deadline) {

    let storypointPerDay = 0;
    for (let i = 0; i < arrPeople.length; i++) {
        storypointPerDay += arrPeople[i];
    }

    let storypointPerHour = storypointPerDay / 8;
    let loopStorypiont = storypointPerDay;

    let storypointPerWork = 0;
    for (let i = 0; i < arrTasks.length; i++) {
        storypointPerWork += arrTasks[i];
    }

    let currentDate = new Date();
    let currentDay = currentDate.getDate();
    let currentMonth = currentDate.getMonth();
    let currentYear = currentDate.getFullYear();


    let checkDate = new Date(currentYear, currentMonth, currentDay);
    let isWorkDone = false;
    let countWorkDays = 1;
    let countLeftDays = 0;

    let dayOfWeek;

    while (checkDate <= deadline) {

        dayOfWeek = checkDate.getDay();

        if (loopStorypiont >= storypointPerWork) {
            countLeftDays++;
            isWorkDone = true;
        }

        if ((dayOfWeek == 1 || dayOfWeek == 2 || dayOfWeek == 3 || dayOfWeek == 4 || dayOfWeek == 5)
            && loopStorypiont < storypointPerWork) {

            countWorkDays++;
            loopStorypiont += storypointPerDay;


        }
        currentDay++;
        checkDate = new Date(currentYear, currentMonth, currentDay);
    }

    countLeftDays = countLeftDays - 1;

    let needAddHours = 0;
    let doneWork = (countWorkDays - 1) * storypointPerDay;

    if (isWorkDone) {
        alert("Your work will be done " + countLeftDays + " days before deadline");
    }
    else {
        needAddHours = (storypointPerWork - doneWork) / storypointPerHour;
        alert("Developer team need additional " + needAddHours + " hours for this work");
    }

}


let arrPeople = [2, 2, 2];
let arrTasks = [1, 3, 6, 10, 16];

let deadline = prompt("Please enter deadline in format dd.mm.yyyy", "");

let dayOfDeadline = Number(deadline.substring(0, 2));
let monOfDeadline = Number(deadline.substring(3, 5)) - 1;
let yearOfDeadline = Number(deadline.substring(6, 10));

let deadlineDate = new Date(yearOfDeadline, monOfDeadline, dayOfDeadline);

checkPossibility(arrPeople, arrTasks, deadlineDate);


