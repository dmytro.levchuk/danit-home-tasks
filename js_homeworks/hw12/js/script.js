btn1.onclick = function () {
    document.body.removeChild(document.getElementById('btn1'));

    let elemForm = document.createElement("form");

    elemForm.setAttribute('id', 'form1');

    let fieldOne = document.createElement('p');
    let textFieldOne = document.createElement('span');
    textFieldOne.innerHTML = 'Please enter an radius in pixels ';
    let inputFieldOne = document.createElement('input');

    inputFieldOne.setAttribute('id', 'fld1');

    let fieldTwo = document.createElement('p');
    let textFieldTwo = document.createElement('span');
    textFieldTwo.innerHTML = 'Please enter a color of circle ';
    let inputFieldTwo = document.createElement('input');
    inputFieldTwo.setAttribute('id', 'fld2');

    let submitBtn = document.createElement('input');
    submitBtn.setAttribute('id', 'sbmBtn');
    submitBtn.setAttribute('type', 'button');
    submitBtn.setAttribute('value', 'And finally we can draw a circle');

    fieldOne.appendChild(textFieldOne);
    fieldOne.appendChild(inputFieldOne);

    fieldTwo.appendChild(textFieldTwo);
    fieldTwo.appendChild(inputFieldTwo);

    elemForm.appendChild(fieldOne);
    elemForm.appendChild(fieldTwo);
    elemForm.appendChild(submitBtn);

    document.body.appendChild(elemForm);
    sbmBtn.onclick = function () {
        let circleDiv = document.createElement('div');
        circleDiv.setAttribute('style', `height: ${fld1.value}px; width: ${fld1.value}px; background-color: ${fld2.value}; border-radius: ${fld1.value / 2}px`);
        document.body.appendChild(circleDiv);
    };


};


btn2.onclick = function () {
    document.body.removeChild(document.getElementById('btn1'));

    let elemForm = document.createElement("form");

    elemForm.setAttribute('id', 'form1');

    let fieldOne = document.createElement('p');
    let textFieldOne = document.createElement('span');
    textFieldOne.innerHTML = 'Please enter an radius in pixels ';
    let inputFieldOne = document.createElement('input');

    inputFieldOne.setAttribute('id', 'fld1');


    let submitBtn = document.createElement('input');
    submitBtn.setAttribute('id', 'sbmBtn');
    submitBtn.setAttribute('type', 'button');
    submitBtn.setAttribute('value', 'And finally we can draw circles');

    fieldOne.appendChild(textFieldOne);
    fieldOne.appendChild(inputFieldOne);

    elemForm.appendChild(fieldOne);
    elemForm.appendChild(submitBtn);

    document.body.appendChild(elemForm);
    sbmBtn.onclick = function () {
        let circles = document.createElement('div');
        circles.setAttribute('id', 'circles');
        circles.setAttribute('style', `width: ${fld1.value * 10}px`);

        for (let i = 0; i < 100; i++) {
            let circleDiv = document.createElement('div');

            circleDiv.setAttribute('style', `float: left; height: ${fld1.value}px; width: ${fld1.value}px; 
            background-color: rgb(${Math.floor((Math.random() * 255))},${Math.floor((Math.random() * 255))},${Math.floor((Math.random() * 255))}); 
            border-radius: ${fld1.value / 2}px`);

            circles.appendChild(circleDiv);

        }
        document.body.appendChild(circles);

        circles.onclick = function (e) {
            circles.removeChild(e.target);

        }
    };


};

